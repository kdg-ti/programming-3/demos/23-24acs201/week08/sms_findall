package be.kdg.java3.relationsdemo.presentation;

import be.kdg.java3.relationsdemo.domain.Student;
import be.kdg.java3.relationsdemo.repository.StudentRepository;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

@Component
public class StudentMenu {
    private Scanner scanner = new Scanner(System.in);
    private StudentRepository studentRepository;

    public StudentMenu(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public void show() {
        while (true) {
            System.out.println("Welcome to the Student Management System");
            System.out.println("========================================");
            System.out.println("1) list all students");
            System.out.println("2) add a student");
            System.out.println("3) update a student");
            System.out.println("4) delete a student");
            System.out.println("5) change address of student");
            System.out.println("6) change school of student");
            System.out.println("7) list all students of school");
            System.out.println("8) delete a school");
            System.out.println("9) add student to course");
            System.out.println("10) list students of course");
            System.out.println("11) list courses of student");
            System.out.println("12) delete course");
            System.out.print("Your choice:");
            int choice = Integer.parseInt(scanner.nextLine());
            switch (choice) {
                case 1 -> listAllStudents();
                case 2 -> addStudent();
                case 3 -> updateStudent();
                case 4 -> deleteStudent();
                case 5 -> changeAddressOfStudent();
                case 6 -> changeSchoolOfStudent();
                case 7 -> showAllStudentsOfSchool();
                case 8 -> deleteSchool();
                case 9 -> addStudentToCourse();
                case 10 -> listStudentsOfCourse();
                case 11 -> listCoursesOfStudent();
                case 12 -> deleteCourse();
            }
        }
    }

    private void deleteCourse() {
        //TODO
    }

    private void listCoursesOfStudent() {
        //TODO
    }

    private void listStudentsOfCourse() {
        //TODO
    }

    private void addStudentToCourse() {
        //TODO
    }

    private void deleteSchool() {
        //TODO
    }

    private void showAllStudentsOfSchool() {
        //TODO
    }

    private void changeSchoolOfStudent() {
        //TODO
    }

    private void changeAddressOfStudent() {
        //TODO
    }

    private void listAllStudents() {
        try {
            studentRepository.findAll().forEach(System.out::println);
        } catch (RuntimeException dbe) {
            System.out.println("Unable to find all students:");
            System.out.println(dbe.getMessage());
        }
    }

    private void changeSchoolOfStudent(Student student){
        //TODO
    }

    private void addStudent() {
        System.out.print("Name:");
        String name = scanner.nextLine();
        System.out.print("Length:");
        double length = Double.parseDouble(scanner.nextLine());
        System.out.print("Birthday (dd-mm-yyyy):");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate birthday = LocalDate.parse(scanner.nextLine(), formatter);
        Student student = new Student(name, length, birthday);
        try {
            Student createdStudent
                    = studentRepository.createStudent(student);
            System.out.println("Student added to database:" + createdStudent);
        } catch (RuntimeException dbe) {
            System.out.println("Problem:" + dbe.getMessage());
        }
    }

    private void updateStudent() {
        listAllStudents();
        System.out.print("Id:");
        int id = Integer.parseInt(scanner.nextLine());
        System.out.print("Name:");
        String name = scanner.nextLine();
        System.out.print("Length:");
        double length = Double.parseDouble(scanner.nextLine());
        System.out.print("Birthday (dd-mm-yyyy):");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate birthday = LocalDate.parse(scanner.nextLine(), formatter);
        Student student = new Student(id, name, length, birthday);
        try {
            studentRepository.updateStudent(student);
        } catch (RuntimeException dbe) {
            System.out.println("Problem:" + dbe.getMessage());
        }
    }

    private void deleteStudent() {
        listAllStudents();
        System.out.print("Id:");
        int id = Integer.parseInt(scanner.nextLine());
        try {
            studentRepository.deleteStudent(id);
        } catch (RuntimeException dbe) {
            System.out.println("Problem:" + dbe.getMessage());
        }
    }
}
