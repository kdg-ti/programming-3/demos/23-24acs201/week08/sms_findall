package be.kdg.java3.relationsdemo.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Student {
	private int id;
	private String name;
	private double lenght;
	private LocalDate birthday;
	private Address address;

	public Student(int id, String name, double lenght, LocalDate birthday, Address address) {
		this(id, name, lenght, birthday);
		setAddress(address);
	}

	public Student(int id, String name, double lenght, LocalDate birthday) {
		this.id = id;
		this.name = name;
		this.lenght = lenght;
		this.birthday = birthday;
	}

	public Student(String name, double lenght, LocalDate birthday) {
		this.name = name;
		this.lenght = lenght;
		this.birthday = birthday;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLenght() {
		return lenght;
	}

	public void setLenght(double lenght) {
		this.lenght = lenght;
	}

	public LocalDate getBirthday() {
		return birthday;
	}

	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}

	@Override
	public String toString() {
		return "Student{" +
			"id=" + id +
			", name='" + name + '\'' +
			", lenght=" + lenght +
			", birthday=" + birthday +
			", " + address +
			'}';
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
		address.setStudent(this);
	}
}
